using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class QuickScripyAssetModifier : UnityEditor.AssetModificationProcessor
{
    public static void OnWillCreateAsset(string assetPath)
    {
        if (!assetPath.EndsWith(".meta"))
        {
            Object obj = AssetDatabase.LoadAssetAtPath<Object>(assetPath);
            if (obj != null && obj.GetType() == typeof(MonoScript))
            {
                GUIContent[] menuOptions = new GUIContent[] { new GUIContent("Open Script Editor") };
                string[] assetGUIDs = new string[] { AssetDatabase.AssetPathToGUID(assetPath) };
                EditorUtility.DisplayCustomMenu(new Rect(0, 0, 0, 0), menuOptions, -1, MyMenuCallback, assetGUIDs);
            }
        }
    }

    private static void MyMenuCallback(object userData, string[] options, int selected)
    {
        if (options[selected] == "Open Script Editor")
        {
            string assetGUID = (string)userData;
            MonoScript script = AssetDatabase.LoadAssetAtPath<MonoScript>(AssetDatabase.GUIDToAssetPath(assetGUID));
            QuickScript window = EditorWindow.GetWindow<QuickScript>();
            window.script = script.text;
        }
    }
}

public class QuickScript : EditorWindow
{
    public string script = "";

    //[MenuItem("QuickScript/Script Editor Window")]
    //public static void ShowWindow()
    //{
    //    GetWindow<QuickScript>("QuickScript Editor");
    //}

    private void OnGUI()
    {
        GUILayout.Label("Script Editor", EditorStyles.boldLabel);

        script = EditorGUILayout.TextArea(script, GUILayout.ExpandHeight(true));

        if (GUILayout.Button("Save"))
        {
            Debug.Log("Saving...");
        }
    }
}
