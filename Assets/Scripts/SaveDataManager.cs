using System.IO;
using UnityEngine;

public static class SaveDataManager
{
    public static string directory = "/SaveData/";
    public static string fileName = "savedata.json";

    public static void Save(GameData data)
    {
        string dir = Application.persistentDataPath + directory;
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        string json = JsonUtility.ToJson(data);
        File.WriteAllText(dir + fileName, json);
    }

    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + fileName;
        GameData data = new GameData();

        if (!File.Exists(fullPath))
        {
            Debug.LogWarning($"SaveDataManager -> file {fullPath} does not exist");
            return data;
        }

        string json = File.ReadAllText(fullPath);
        data = JsonUtility.FromJson<GameData>(json);
        return data;
    }
}
