using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Weapon
{
    public string weaponName;
    public int damage;
    public int weight;
}
