using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public static class BinarySaveManager
{
    public static string directory = "/SaveData/";
    public static string fileName = "savedata.bin";

    public static void Save(GameData data)
    {
        string dir = Application.persistentDataPath + directory;
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        BinaryFormatter bin = new BinaryFormatter();
        using (FileStream file = File.Create(dir + fileName))
        {
            bin.Serialize(file, data);
        }
    }

    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + fileName;
        GameData data = new GameData();

        if (!File.Exists(fullPath))
        {
            Debug.LogWarning($"SaveDataManager -> file {fullPath} does not exist");
            return data;
        }

        BinaryFormatter bin = new BinaryFormatter();
        using (FileStream file = File.OpenRead(fullPath))
        {
            data = (GameData)bin.Deserialize(file);
        }
        return data;
    }
}
