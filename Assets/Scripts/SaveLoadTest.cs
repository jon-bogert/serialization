using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadTest : MonoBehaviour
{
    enum Type { Binary, JSON };
    [SerializeField] Type type;
    public GameData data;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            switch (type)
            {
                case Type.JSON:
                    SaveDataManager.Save(data);
                    break;
                case Type.Binary:
                    BinarySaveManager.Save(data);
                    break;

            }
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            switch (type)
            {
                case Type.JSON:
                    data = SaveDataManager.Load();
                    break;
                case Type.Binary:
                    BinarySaveManager.Load();
                    break;
            }
        }
    }
}
