using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public string playerName;
    public int currentLevel;
    public int savePointID;

    public Weapon currentEquippedWeapon;
    public Armor currentEquippedArmor;

    public List<Item> inventory;
    public List<Skill> skills;
}
