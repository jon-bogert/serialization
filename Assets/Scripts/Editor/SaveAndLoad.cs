using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SaveLoadTest))]
public class SaveAndLoad : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SaveLoadTest saveUI = (SaveLoadTest)target;
        if (GUILayout.Button("Save to File"))
        {
            SaveDataManager.Save(saveUI.data);
        }
        if (GUILayout.Button("Load from File"))
        {
            saveUI.data = SaveDataManager.Load();
        }
    }
}
