using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Weapon> weapons = new List<Weapon>();
    public int itemSlots;

    [SerializeField] GameObject bagReference;
    public int startingItemSlots = 0;
    public Vector3 position = Vector3.zero;

}